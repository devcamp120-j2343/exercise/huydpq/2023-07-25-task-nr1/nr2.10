// khai báo thư viện express
const express = require("express");

// khai bao thu vien mongoose
var mongoose = require('mongoose');


//khai báo mongoose
const reviewSchema = require("./app/models/reviewModel")
const courseSchema = require("./app/models/courseModel")
// khai báo router
const { getCoures } = require("./app/middlewares/courseMiddleware");
const { CourseRouter } = require("./app/routes/courseRoute");
const { reviewRouter } = require("./app/routes/reviewRoute");

// khỏi tại appp
const app = express();

// khỏi tại port
const port = 8000; 

// ket noi mongoose
// mongoose.connect("mongodb://localhost:27017/CRUD_Course", function(error) {
//     if (error) throw error;
//     console.log('Successfully connected');
//    })
   mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course")
  .then(() => console.log("Connected to Mongo Successfully"))
  .catch(error => handleError(error));

   
app.use(express.json())
// app.use("/", (req, res, next) =>{
//     console.log(new Date());
//     next()
// }, (req, res, next) =>{
//     console.log(new Date());
//     next()
// })

// app.post("/", (req, res, next) => {
//     console.log(req.method)
//     next();
// })

// app.get("/", (req, res) => {
//     let today = new Date();
//     res.json({
//         message: `Hom nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`
//     })
// })
app.use("/",CourseRouter)
app.use("/",reviewRouter)

app.listen(port, () =>{
    console.log(`Đang chay trên cổng:  ${port}`)
})