const reviewModel = require("../models/reviewModel")

const courseModel = require("../models/courseModel");
const { default: mongoose } = require("mongoose");

// get all review
const getAllReview = async (req, res) => {
    console.log("Get All Review");
    let courseId = req.query.courseId;

    if (courseId !== undefined && !mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Course ID is not valid"
        })
    }

    try {
        if (courseId === undefined) {
            const reviewList = await reviewModel.find();

            if (reviewList && reviewList.length > 0) {
                return res.status(200).json({
                    status: "Get all reviews sucessfully",
                    data: reviewList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any review"
                })
            }

        }else {
            const courseInfo = await courseModel.findById(courseId).populate("reviews");

            return res.status(200).json({
                status: "Get all reviews of course sucessfully",
                data: courseInfo.reviews
            })
        }


    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })

    }

}
// get review by ID

const getReviewByID = async (req, res) => {
    let id = req.params.reviewId

    if(!mongoose.Types.ObjectId.isValid(id) && id === undefined){
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const reviewInfo = await reviewModel.findById(id);

        if (reviewInfo) {
            return res.status(200).json({
                status:"Get review by id sucessfully",
                data: reviewInfo
            })
        } else {
            return res.status(404).json({
                status:"Not found any review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }


}
// Create Review
const createReview = async(req, res) => {
    // thu thập dữ liệu
    let body = req.body
    //--- kiểm tra -------
    // Star là số nguyên, lớn hơn 0 và nhỏ hơn 6
    // Number.isInteger(stars) && stars > 0 && stars < 6
    if (body.stars === undefined || !(Number.isInteger(body.stars) && body.stars > 0 && body.stars < 6)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Star is not valid"
        })
    }
    // B3: Thao tác với CSDL
    var newReview = {
        _id: new mongoose.Types.ObjectId(),
        stars: body.stars,
        note: body.note
    }

    try {
        const createdReview = await reviewModel.create(newReview);

        const updatedCourse = await courseModel.findByIdAndUpdate(body._id, {
            $push: { reviews: createdReview._id }
        })

        return res.status(201).json({
            status: "Create review successfully",
            course: updatedCourse,
            data: createdReview
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }



}

// update Review 
const updateReview = async (req, res) => {
    let id = req.params.reviewId
    let body = req.body

    if(!mongoose.Types.ObjectId.isValid(id) && id === undefined){
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    try {
        let newReview = {
            stars: body.stars,
            note: body.note
        }
        const updateReview = await reviewModel.findByIdAndUpdate(id, newReview)

        if(updateReview) {
            return res.status(200).json({
                status:"Update review sucessfully",
                data: updateReview

            })
        } else {
            return res.status(404).json({
                status:"Not found any review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })

    }
}

// delete REview 

const deleteReview = async (req, res) => {
    let id = req.params.reviewId

    // Nếu muốn xóa id thừa trong mảng review thì có thể truyền thêm coureId để xóa
    var courseId = req.query.courseId;

    if(!mongoose.Types.ObjectId.isValid(id) && id === undefined){
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    try {   
         // Nếu có courseId thì xóa thêm (optional)
         if(courseId !== undefined){
           await courseModel.findByIdAndUpdate(courseId, {
                $pull : {reviews: id}
            })
         }

         const deleteReview = await reviewModel.findByIdAndDelete(id)

         if(deleteReview){
            return res.status(200).json({
                message: `Delete reivew sucessfully`,
                deleteReview
            })
         } else {
            return res.status(404).json({
                message: `not found any review`,
                
            })
         }
        
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }

}
 module.exports = {getAllReview, createReview,getReviewByID, updateReview, deleteReview }