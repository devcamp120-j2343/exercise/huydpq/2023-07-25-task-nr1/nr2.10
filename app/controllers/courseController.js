const { default: mongoose } = require("mongoose");
const courseModel = require("../models/courseModel");

const reviewModel = require("../models/reviewModel");


// get all course
const getAllCourse = (req, res) => {
    let coursename = req.query.coursename
    let minStudent = req.query.minStudent
    let maxStudent = req.query.maxStudent

    let condition = {};
    if(coursename){
        condition.title = coursename
    }
    if(minStudent){
        condition.noStudent = {$gte: minStudent }
    }
    if(maxStudent){
        condition.noStudent = {...condition.noStudent, $lte : maxStudent}
    }
    console.log(condition)
    courseModel.find(condition)
        .then((data) => {
            return res.status(201).json({
                message: "Get all courses sucessfully",
                courses: data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}
// get a course 
const getCourseById = (req, res) => {
    //B1: thu thập dữ liệu
    let courseId = req.params.courseid

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    courseModel.findById(courseId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Get course by id sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}

// Create a Course
const createCourse = (req, res) => {
    //B1:  thu thập dữ liệu
    let body = req.body

    //B2:  kiểm tra thông tin
    if (!body.title) {
        return res.status(400).json({
            message: `title is required`
        })
    }
    if (!Number.isInteger(body.noStudent) || body.noStudent < 0) {
        return res.status(400).json({
            message: `student is invalid`
        })
    }
    // B3: xủ lý kêt quả
    let newCourse = {
        _id: new mongoose.Types.ObjectId(),
        title: body.title,
        description: body.description,
        noStudent: body.noStudent
    }
    courseModel.create(newCourse)
        .then(data => {
            return res.status(200).json({
                message: "Create a Course Sucessfully",
                data
            })
        })
        .catch(error => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

// Update a Course
const updateCourse = (req, res) => {
    console.log("update a Course")
    // B1: Thu thập dữ liệu
    let id = req.params.courseid
    let body = req.body
    // kiểm tra thông tin
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (!body.title) {
        return res.status(400).json({
            message: `title is required`
        })
    }
    if (!Number.isInteger(body.noStudent) || body.noStudent < 0) {
        return res.status(400).json({
            message: `student is invalid`
        })
    }
    // xử lý kết quả
    let newUpdateCourse = {

        title: body.title,
        description: body.description,
        noStudent: body.noStudent
    }

    courseModel.findByIdAndUpdate(id, newUpdateCourse)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Update course sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })


    console.log(body)


}

// Update a Course
const deleteCourse = (req, res) => {
    let id = req.params.courseid

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    courseModel.findByIdAndDelete(id)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Delete course sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}


module.exports = { getAllCourse, getCourseById, createCourse, updateCourse, deleteCourse }

